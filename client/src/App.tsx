import React from "react";
import CharacterGrid from "./components/charactersGrid";

const App: React.FC = () => {
  return (
    <div className="App">
      <CharacterGrid />
    </div>
  );
};

export default App;
