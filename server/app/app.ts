import express from "express";
import routes from "./routes";
import cors from "cors";

const app: express.Application = express();

app.use(cors());

app.use("/", routes);

app.listen(process.env.APP_SERVER_PORT, function() {
  console.log(`Example app listening on port ${process.env.APP_SERVER_PORT}!`);
});

export default app;
