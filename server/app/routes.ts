import { Router, Request, Response } from "express";
import { query, validationResult } from "express-validator";

import { getCharacterByName } from "./controllers/character";
import ISearchResult from "./types/SearchResult";

const router = Router();

router.get("/", (_req: Request, res: Response) => {
  res.send("Hello World test!");
});

router.get(
  "/character",
  [
    query("search")
      .isString()
      .optional(),
    query("page").isNumeric(),
    query("limit").isNumeric(),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(400).json({ error: `Bad parameters`, fields: errors });
    }
    const results = (await getCharacterByName(req.query)) as ISearchResult;
    if (results.code) {
      return res.status(500).json({ error: `${results.code} : ${results.message}` });
    }
    return res.json(results);
  }
);

export default router;
