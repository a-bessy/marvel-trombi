import React, { useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import ReactPaginate from "react-paginate";
import Loader from "react-loader-spinner";

import SearchForm from "../components/searchForm";
import CharacterTile from "../components/characterTile";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

interface Character {
  name: string;
  thumbnail: { path: string; extension: string };
}

const OuterGrid = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px;
  hr {
    margin: 10px 0;
  }
`;

const Paginator = styled.div`
  display: flex;
  justify-content: center;
  height: 50px;
  align-items: center;
  margin: 10px 0;
  ul {
    margin: 0;
    padding: 0;
    li {
      display: inline-block;
      text-decoration: none;
      padding: 10px;

      &:hover,
      &.selected {
        text-decoration: underline;
        font-weight: bold;
        cursor: pointer;
      }
    }
  }
`;

const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: flex-start;
  min-height: 500px;
`;

const LIMIT = 20;
const API_URL = `${process.env.REACT_APP_API_HOST}:${process.env.REACT_APP_API_PORT}`;

const CharacterGrid: React.FC = () => {
  const [characters, setCharacters] = useState<Character[]>([]);
  const [page, setPage] = useState<number>(1);
  const [pageCount, setPageCount] = useState<number>(0);
  const [currentSearch, setCurrentSearch] = useState<string>("");
  const [searchType, setSearchType] = useState<string>("fullname");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [fetchError, setFetchError] = useState<string | null>(null);

  const fetchCharacters = useCallback(async () => {
    setIsLoading(true);
    const endpoint = `/character?page=${page}&limit=${LIMIT}`;
    const searchParams =
      currentSearch !== ""
        ? `&searchType=${searchType}&search=${currentSearch}`
        : "";

    const searchResult = await fetch(
      `${API_URL}${endpoint}${searchParams}`
    ).then(res => res.json());
    if (searchResult.error) {
      setFetchError(`Une erreur s'est produite : ${searchResult.error}`);
    } else {
      setCharacters(searchResult.results);
      setPageCount(Math.ceil(searchResult.total / LIMIT));
    }
    setIsLoading(false);
  }, [
    page,
    searchType,
    currentSearch,
    setCharacters,
    setPageCount,
    setIsLoading
  ]);

  useEffect(() => {
    fetchCharacters();
  }, [fetchCharacters]);

  return (
    <OuterGrid>
      <SearchForm
        onSearch={async (search, searchType) => {
          setPage(1);
          setCurrentSearch(search);
          setSearchType(searchType);
        }}
      />
      <hr />
      {fetchError && <div>{fetchError}</div>}
      {isLoading ? (
        <Grid>
          <Loader type="Circles" color="lightblue" />
        </Grid>
      ) : (
        <Grid>
          {characters.length > 0
            ? characters.map(character => (
                <CharacterTile key={character.name} character={character} />
              ))
            : `Aucun résultat pour la recherche : ${currentSearch} (${
                searchType === "fullname" ? "Nom entier" : "Commençant par"
              })`}
        </Grid>
      )}
      <hr />
      {pageCount > 1 && (
        <Paginator>
          <ReactPaginate
            pageCount={pageCount}
            pageRangeDisplayed={5}
            marginPagesDisplayed={5}
            initialPage={page - 1}
            onPageChange={({ selected }) => {
              setPage(selected + 1);
            }}
          />
        </Paginator>
      )}
    </OuterGrid>
  );
};

export default CharacterGrid;
