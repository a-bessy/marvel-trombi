import md5 from "md5";
import fetch from "node-fetch";
import ISearchResult from "../types/SearchResult";

interface QueryTypes {
  search: string;
  searchType: string;
  page: number;
  limit: number;
}

const { MARVEL_API_PUBLIC_KEY, MARVEL_API_PRIVATE_KEY } = process.env;

const URL_BASE = "http://gateway.marvel.com/v1/public/characters";

const queryParams: { [key: string]: string } = {
  fullname: "name",
  startsWith: "nameStartsWith",
};

export async function getCharacterByName(query: QueryTypes) {
  const { search, searchType, page, limit } = query;
  const timestamp = Date.now();
  const hash = md5(`${timestamp}${MARVEL_API_PRIVATE_KEY}${MARVEL_API_PUBLIC_KEY}`);
  const auth = `apikey=${MARVEL_API_PUBLIC_KEY}&ts=${timestamp}&hash=${hash}`;
  const pagination = `offset=${(page - 1) * limit}&limit=${limit}`;
  const url = search
    ? `${URL_BASE}?${auth}&${pagination}&${queryParams[searchType]}=${search}`
    : `${URL_BASE}?${auth}&${pagination}`;

  const searchResult = await fetch(url)
    .then(res => res.json())
    .then((res: ISearchResult) => res);

  if (searchResult.code !== 200) {
    return searchResult;
  } else {
    const { results, total } = searchResult.data;
    return {
      results: results.map(({ name, thumbnail }) => ({ name, thumbnail })),
      total,
    };
  }
}
