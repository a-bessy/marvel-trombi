Etapes pour lancer l'application en mode dev :
1. Créer un fichier .env à la racine du projet contenant les clé suivantes :

MARVEL_API_PUBLIC_KEY=
MARVEL_API_PRIVATE_KEY=
APP_SERVER_PORT=
REACT_APP_PORT=
API_HOST=http://localhost

Les clés MARVEL_API_PUBLIC_KEY et MARVEL_API_PRIVATE_KEY doivent être générées via le site de marvel (https://developer.marvel.com/)
Les clés APP_SERVER_PORT et REACT_APP_PORT correspondent aux ports utilisés respectivmement par l'api et par le serveur front.

2. Lancer la commande : docker-compose up --build

3. Dans un navigateur, ouvrir l'url : http://localhost:REACT_APP_PORT