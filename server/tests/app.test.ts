import app from "../app/app";
import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import "mocha";

chai.use(chaiHttp);
describe("Server!", () => {
  it("Returns some dumb text", done => {
    chai
      .request(app)
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.text).to.equals("Hello World test!");
        done();
      });
  });

  it("Returns some marvel characters", done => {
    chai
      .request(app)
      .get("/character?page=1&limit=10")
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.results).to.be.an("array");
        res.body.results.map(
          (result: {
            name: string;
            thumbnail: {
              path: string;
              extension: string;
            };
          }) => {
            expect(result).to.have.all.keys("name", "thumbnail");
            expect(result.thumbnail).to.have.all.keys("path", "extension");
          }
        );
        expect(res.body.total).to.be.a("number");
        done();
      });
  });

  it("Should return err 400 because query is invalid", done => {
    chai
      .request(app)
      .get("/character?page=one&limit=ten")
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });
});
