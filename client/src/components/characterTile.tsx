import React from "react";
import styled from "styled-components";

interface Props {
  character: {
    name: string;
    thumbnail: {
      path: string;
      extension: string;
    };
  };
}

const IMG_SIZE = "landscape_medium";

const Tile = styled.div`
  width: 20%;
  display: flex;
  justify-content: center;
  h3 {
    width: 220px;
    margin: 3px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }
  div {
    margin: 5px;
    text-align: center;
    padding: 10px;
    border: 1px solid lightgray;
    border-radius: 10px;
    width: 220px;
  }
`;

const CharacterTile: React.FC<Props> = ({ character }) => {
  return (
    <Tile>
      <div>
        <img
          alt={`${character.name} profile pic`}
          src={`${character.thumbnail.path}/${IMG_SIZE}.${character.thumbnail.extension}`}
        />
        <h3>{character.name}</h3>
      </div>
    </Tile>
  );
};

export default CharacterTile;
