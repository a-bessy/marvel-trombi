import React, { useState } from "react";
import styled from "styled-components";

interface Props {
  onSearch: (search: string, searchType: string) => void;
}

const Form = styled.div`
  display: flex;
  input[name="search"] {
    height: 30px;
    width: 500px;
    padding: 5px;
  }
  align-items: center;
  justify-content: center;
`;

const Radio = styled.div`
  display: flex;
  justify-content: flex-start;
  margin: 0 10px;
  input {
    margin: 0 10px;
    height: 1.5em;
    width: 1.5em;
    &:hover {
      cursor: pointer;
    }
  }
`;

let searchTimeout: number;
const SearchForm: React.FC<Props> = ({ onSearch }) => {
  const [search, setSearch] = useState<string>("");
  const [searchType, setSearchType] = useState<string>("fullname");
  return (
    <Form>
      <input
        name="search"
        placeholder="Nom du héros..."
        value={search}
        onChange={({ target: { value } }) => {
          clearTimeout(searchTimeout);
          setSearch(value);
          searchTimeout = window.setTimeout(
            () => onSearch(value, searchType),
            500
          );
        }}
      />
      <Radio>
        <input
          name="search-type"
          type="radio"
          value="fullname"
          checked={searchType === "fullname"}
          onChange={() => {
            setSearchType("fullname");
            onSearch(search, "fullname");
          }}
        />
        <label>Nom entier</label>
      </Radio>
      <Radio>
        <input
          name="search-type"
          type="radio"
          value="startsWith"
          checked={searchType === "startsWith"}
          onChange={() => {
            setSearchType("startsWith");
            if (search !== "") {
              onSearch(search, "startsWith");
            }
          }}
        />
        <label>Commençant par</label>
      </Radio>
    </Form>
  );
};

export default SearchForm;
